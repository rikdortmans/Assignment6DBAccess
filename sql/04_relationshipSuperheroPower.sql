

/*Third table*/ 
create table SuperheroPower
(
 SuperheroId int,
 PowerId int,

 
 /*Primary Key is Id*/
 Primary Key (SuperheroId,PowerId)
);


/*Add foreign key for SuperheroPower*/

Alter table SuperheroPower
add foreign key (SuperheroId) references Superhero(Id)
Alter table SuperheroPower
add foreign key (PowerId) references Power(Id)
