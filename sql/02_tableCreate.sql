
/*First table*/
create table Superhero
(
 Id int Identity(1,1),
 Name nvarchar (50),
 Alias nvarchar (100),
 Origin nvarchar (50),
 
 /*Primary Key is Id*/
 Primary Key (id)
);

/*Second table*/
create table Assistant 
(
 Id int Identity(1,1),
 Name nvarchar (50),
 SuperheroId int,
 
 /*Primary Key is Id*/
 Primary Key (id)
);

/*fourth table*/
create table Power
(
 Id int Identity(1,1),
 Name nvarchar (50),
 Description nvarchar (300),

 
 /*Primary Key is Id*/
 Primary Key (id)
);

