# assignment6dbaccess

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

A common line project build with C#. Has several methods to retrieve data from the local Chinook database. All methods are summarized.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [License](#license)

## Install

Fork the repository and open in visual studio 2022. You need the Chinook database provided by Noroff to retrieve any data.

## Usage

Run the project.

## Maintainers

[@rikdortmans](https://gitlab.com/rikdortmans)
[@VictorRosaValdez](https://gitlab.com/VictorRosaValdez)
[@leroy96](https://gitlab.com/Leroy96)



Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Rik Dortmans
