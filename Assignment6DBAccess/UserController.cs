﻿using Assignment6DBAccess.DataAccess;
using Assignment6DBAccess.DataAccess.SQL;
using Assignment6DBAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6DBAccess
{
    internal class UserController
    {
        IUserDataAccess userDataAccess;
        ICustomerProgramRepository customerRepo;

        public UserController()
        {
            userDataAccess = new UserSQLDataAccess();
            customerRepo = new CustomerRepository(userDataAccess);
        }

        /// <summary>
        /// Gets all the customers from Chinook database
        /// </summary>
        public void ReadAllCustomers()
        {
            Console.WriteLine("2.1 Results");
            List<Customer> customers = customerRepo.ReadAllCustomers();

            foreach (Customer customer in customers)
            {
                Console.WriteLine(customer.ToString());
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Gets specific customer from Chinook database by customer id
        /// </summary>
        /// <param name="customerId">Id from the customer to retrieve</param>
        public void ReadCustomersById(int customerId)
        {
            Console.WriteLine("2.2 Results");
            Customer customerById = customerRepo.ReadCustomerByID(customerId);
            Console.WriteLine(customerById.ToString());
            Console.WriteLine();
        }

        /// <summary>
        /// Gets customers by partial first name
        /// </summary>
        /// <param name="name">Partial first name of customer</param>
        public void ReadCustomerByName(string name)
        {
            Console.WriteLine("2.3 Results");
            List<Customer> customersByName = customerRepo.ReadCustomerByName(name);
            foreach (Customer customerByName in customersByName)
            {
                Console.WriteLine(customerByName.ToString());
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Gets a page of customers, based on a limit to the page and an offset
        /// </summary>
        /// <param name="limit">Limit of the page, this the the amount of customers returned</param>
        /// <param name="offset">Starting point of the page</param>
        public void ReadCustomerPage(int limit, int offset)
        {
            Console.WriteLine("2.4 Results");
            List<Customer> customersPage = customerRepo.ReadCustomerPage(limit, offset);
            foreach (Customer customerInPage in customersPage)
            {
                Console.WriteLine(customerInPage.ToString());
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Create user with first name, last name, and email and stores the user in the Chinook Database
        /// </summary>
        /// <param name="firstName">First name of the customer</param>
        /// <param name="lastName">Last name of the customer</param>
        /// <param name="email">Email of the customer</param>
        public void CreateUser(string firstName, string lastName, string email)
        {
            Console.WriteLine("2.5 Results");
            Customer customerToCreate = new Customer(firstName, lastName, email);
            bool result = customerRepo.CreateCustomer(customerToCreate);
            Console.WriteLine("Create customer result: " + result);
            Console.WriteLine();
        }

        public void UpdateUser(Customer customerToUpdate)
        {
            Console.WriteLine("2.6 Results");
            customerToUpdate.State = "Limburg";
            customerToUpdate.Company = "Test Inc. Ltd.";
            customerToUpdate.Country = "Netherlands";
            bool result = customerRepo.UpdateCustomer(customerToUpdate.Id, customerToUpdate);
            Console.WriteLine("Update customer result: " + result);
            Console.WriteLine();
        }

        /// <summary>
        /// Gets all users in each country ordered descending
        /// </summary>
        public void ReadNumberCustomersByCountry()
        {
            Console.WriteLine("2.7 Results");
            List<CustomerCountry> customerCountryList = customerRepo.ReadNumberOfCustomersByCountry();

            foreach (CustomerCountry country in customerCountryList)
            {
                Console.WriteLine($"{country.CountryName}: {country.NumberOfCustomers}");
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Returns all the customers ordered by invoice amount. The customer who has spend the most will be first, lowest last.
        /// </summary>
        public void ReadAllCustomersByInvoice()
        {
            Console.WriteLine("2.8 Results");
            List<CustomerSpender> customerSpenders = customerRepo.ReadAllCustomersByInvoiceAmount();

            foreach (CustomerSpender spender in customerSpenders)
            {
                Console.WriteLine($"{spender.FirstName} {spender.LastName}: {spender.Spend}");
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Returns the most popular genre of the customerby customerId. Returns mutliple genres if a tie occurs.
        /// </summary>
        /// <param name="customerId">Id of the customer whos genre to retrieve</param>
        public void ReadCustomerPopularGenre(int customerId)
        {
            Console.WriteLine("2.9 Results");
            List<CustomerGenre> customerGenres = customerRepo.ReadPopularGenreOfCustomerById(customerId);

            foreach (CustomerGenre customer in customerGenres)
            {
                Console.WriteLine($"{customer.FirstName} {customer.LastName} {customer.GenreName} {customer.TrackTotal}");
            }
        }
    }
}
