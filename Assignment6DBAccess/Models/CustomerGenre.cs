﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6DBAccess.Models
{
    internal class CustomerGenre
    {
        public int TrackTotal { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string GenreName { get; set; }
    }
}
