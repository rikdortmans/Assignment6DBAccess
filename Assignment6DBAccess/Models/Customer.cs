﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6DBAccess.Models
{
    internal class Customer
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string? Company { get; set; }
        public string? Address { get; set; } 
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Country { get; set; }
        public string? PostalCode { get; set; }
        public string? Phone { get; set; }
        public string? Fax { get; set; }
        public string Email { get; set; }
        public int? SupportRepId { get; set; }

        public Customer()
        {
        }

        public Customer(string firstname, string lastName, string email)
        {
            Firstname = firstname;
            LastName = lastName;
            Email = email;
        }


        public override string? ToString()
        {
            return $"{Id} {Firstname} {LastName} {Country} {PostalCode} {Phone} {Email}";
        }
    }
}
