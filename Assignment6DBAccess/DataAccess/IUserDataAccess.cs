﻿using Assignment6DBAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6DBAccess.DataAccess
{
    internal interface IUserDataAccess
    {
        List<Customer> ReadAllCustomers();
        Customer ReadCustomerByID(int id);
        List<Customer> ReadCustomerByName(string name);
        List<Customer> ReadCustomerPage(int limit, int offset);
        bool CreateCustomer(Customer customer);
        bool UpdateCustomer(int id, Customer customer);
        List<CustomerCountry> ReadNumberOfCustomersByCountry();
        List<CustomerSpender> ReadAllCustomersByInvoiceAmount();
        List<CustomerGenre> ReadPopularGenreOfCustomerById(int customerId);
    }
}
