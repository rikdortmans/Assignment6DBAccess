﻿using Assignment6DBAccess.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6DBAccess.DataAccess.SQL
{
    internal class UserSQLDataAccess : IUserDataAccess
    {
        SqlConnectionStringBuilder builder;
        public UserSQLDataAccess()
        {
            builder = new SqlConnectionStringBuilder();
            builder.DataSource = "localhost\\SQLEXPRESS";
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;
            builder.Encrypt = false;
        }

        /// <summary>
        /// Create new user
        /// </summary>
        /// <param name="customer">Customer to be created and saved in database</param>
        /// <returns>True if creation was successful, false if not</returns>
        /// <exception cref="Exception">Writes exception into console window and returns false</exception> 
        public bool CreateCustomer(Customer customer)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = $"INSERT INTO Customer (FirstName, LastName, Email) VALUES (@firstname, @lastname, @email)";

                    SqlCommand command = new SqlCommand(sql, connection);
                    command.Parameters.AddWithValue("@firstname", customer.Firstname);
                    command.Parameters.AddWithValue("@lastname", customer.LastName);
                    command.Parameters.AddWithValue("@email", customer.Email);

                    command.ExecuteNonQuery();
                }

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Gets all customers from database
        /// </summary>
        /// <returns>All customers as a list</returns>
        /// <exception cref="Exception">Writes exception into console window and returns null</exception> 
        public List<Customer> ReadAllCustomers()
        {
            List<Customer> customers = new List<Customer>();

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "SELECT * FROM customer";

                    using(SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.Id = reader.GetInt32(0);
                                customer.Firstname = reader.GetString(1);
                                customer.LastName = reader.GetString(2);

                                if (!reader.IsDBNull(7))
                                {
                                    customer.Country = reader.GetString(7);
                                }

                                if (!reader.IsDBNull(8))
                                {
                                    customer.PostalCode = reader.GetString(8);
                                }
                                if(!reader.IsDBNull(9))
                                {
                                    customer.Phone = reader.GetString(9);
                                }
                                customer.Email = reader.GetString(11);

                                customers.Add(customer);
                            }
                        }
                    }
                }

                return customers;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Selects all invoices, names, and id's from all customers. Ordered by highest invoice total.
        /// </summary>
        /// <returns>All total values of all the invoices of all customers.</returns>
        /// <exception cref="Exception">Writes exception into console window and returns null</exception>
        public List<CustomerSpender> ReadAllCustomersByInvoiceAmount()
        {
            List<CustomerSpender> customerSpenders = new List<CustomerSpender>();

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "SELECT SUM(Total), Customer.CustomerId, Customer.FirstName, Customer.LastName " +
                        "FROM Invoice RIGHT JOIN Customer ON Customer.CustomerId = Invoice.CustomerId " +
                        "GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName " +
                        "ORDER BY SUM(Total) DESC";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender spender = new CustomerSpender();

                                if (!reader.IsDBNull(0))
                                {
                                    spender.Spend = reader.GetDecimal(0);

                                }
                                spender.CustomerId = reader.GetInt32(1);
                                spender.FirstName = reader.GetString(2);
                                spender.LastName = reader.GetString(3);

                                customerSpenders.Add(spender);
                            }
                        }
                    }

                    return customerSpenders;
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

        }

        /// <summary>
        /// Gets specific customer based on customer id
        /// </summary>
        /// <param name="id">Id of the customer to retrive</param>
        /// <returns>Customer</returns>
        /// <exception cref="Exception">Writes exception into console window and return null</exception>"
        public Customer ReadCustomerByID(int id)
        {
            try
            {
                Customer customer = new Customer();
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = $"SELECT * FROM Customer WHERE CustomerId={id}";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                
                                customer.Id = reader.GetInt32(0);
                                customer.Firstname = reader.GetString(1);
                                customer.LastName = reader.GetString(2);

                                if (!reader.IsDBNull(7))
                                {
                                    customer.Country = reader.GetString(7);
                                }

                                if (!reader.IsDBNull(8))
                                {
                                    customer.PostalCode = reader.GetString(8);
                                }
                                if (!reader.IsDBNull(9))
                                {
                                    customer.Phone = reader.GetString(9);
                                }
                                customer.Email = reader.GetString(11);
                            }
                        }
                    }
                    return customer;
                }
                    
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets specific customer(s) based on a partial first name.
        /// </summary>
        /// <param name="name">Partial firstname of the customer</param>
        /// <returns>A list of customers, could have multiple partial matches</returns>
        /// <exception cref="Exception">Writes exception into console window and returns null</exception>"
        public List<Customer> ReadCustomerByName(string name)
        {
            name = name.Replace(" ", "");
            List<Customer> customers = new List<Customer> ();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = $"SELECT * FROM Customer WHERE FirstName LIKE '%{name}%'";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.Id = reader.GetInt32(0);
                                customer.Firstname = reader.GetString(1);
                                customer.LastName = reader.GetString(2);

                                if (!reader.IsDBNull(7))
                                {
                                    customer.Country = reader.GetString(7);
                                }

                                if (!reader.IsDBNull(8))
                                {
                                    customer.PostalCode = reader.GetString(8);
                                }
                                if (!reader.IsDBNull(9))
                                {
                                    customer.Phone = reader.GetString(9);
                                }
                                customer.Email = reader.GetString(11);

                                customers.Add(customer);
                            }
                        }
                    }
                    return customers;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets a certain amount of users based on the limit and starts retrieving from the database based on offset. Offset does not equal customer id
        /// but the place in the tabel row.
        /// </summary>
        /// <param name="limit">The amount of customers to retrieve</param>
        /// <param name="offset">Starting point</param>
        /// <returns>Returns a set of customres based on the limit and offset provided. A page of customers essentially.</returns>
        /// <exception cref="Exception">Writes exception into console window and returns null</exception>"
        public List<Customer> ReadCustomerPage(int limit, int offset)
        {
            List<Customer> customers = new List<Customer>();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    
                    string sql = $"SELECT * FROM Customer ORDER BY CustomerId OFFSET {offset} ROWS FETCH NEXT {limit} ROWS ONLY";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.Id = reader.GetInt32(0);
                                customer.Firstname = reader.GetString(1);
                                customer.LastName = reader.GetString(2);

                                if (!reader.IsDBNull(7))
                                {
                                    customer.Country = reader.GetString(7);
                                }

                                if (!reader.IsDBNull(8))
                                {
                                    customer.PostalCode = reader.GetString(8);
                                }
                                if (!reader.IsDBNull(9))
                                {
                                    customer.Phone = reader.GetString(9);
                                }
                                customer.Email = reader.GetString(11);

                                customers.Add(customer);
                            }
                        }
                    }
                    return customers;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Selects the amount of users in each country and orders the list in descending order based on the amount of customers
        /// </summary>
        /// <returns>Returns a list contain country names and the number of customers in said country in descending order</returns>
        /// <exception cref="Exception">Writes exception into console window and returns null</exception>"
        public List<CustomerCountry> ReadNumberOfCustomersByCountry()
        {

            List<CustomerCountry> results = new List<CustomerCountry>();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "SELECT COUNT(CustomerId), Country FROM Customer GROUP BY Country ORDER BY COUNT(CustomerId) DESC";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry customerCountry = new CustomerCountry();
                                customerCountry.NumberOfCustomers = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                {
                                    customerCountry.CountryName = reader.GetString(1);
                                }
                                else
                                {
                                    customerCountry.CountryName = "Unknown";
                                }
                                results.Add(customerCountry);
                            }
                        }
                    }
                }

                return results;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

        }


        /// <summary>
        /// For a given customer, this method will retrieve their most popular genre.
        /// </summary>
        /// <param name="customerId">The customer whos popular genre to retrieve</param>
        /// <returns></returns>
        /// <exception cref="Exception">Writes exception into console window and returns null</exception>"
        public List<CustomerGenre> ReadPopularGenreOfCustomerById(int customerId)
        {
            List<CustomerGenre> results = new List<CustomerGenre>();

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "SELECT Count(Genre.Name) TrackTotal, FirstName, LastName, Genre.Name GenreName " +
                        "FROM Customer " +
                        "RIGHT JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                        "RIGHT JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                        "RIGHT JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                        $"RIGHT JOIN Genre ON Track.GenreId = Genre.GenreId where Customer.CustomerId = {customerId} " +
                        "GROUP BY FirstName, LastName, Genre.Name " +
                        "ORDER BY TrackTotal DESC";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre customerGenre = new CustomerGenre();
                                customerGenre.TrackTotal = reader.GetInt32(0);
                                customerGenre.FirstName = reader.GetString(1);
                                customerGenre.LastName = reader.GetString(2);
                                customerGenre.GenreName = reader.GetString(3);

                                results.Add(customerGenre);
                            }
                        }
                    }
                }

                return results;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Update the given customers new values
        /// </summary>
        /// <param name="id">Id of the customer</param>
        /// <param name="customer">Customer with updated values</param>
        /// <returns>True if update was succesful</returns>
        /// <exception cref="Exception">Writes exception into console window and returns false</exception>"
        public bool UpdateCustomer(int id, Customer customer)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = $"UPDATE Customer SET FirstName = @firstname, LastName = @lastname, Company = @company, Address = @address, City = @city, State = @state, Country = @country, PostalCode = @postalcode, Phone = @phone, Fax = @fax, Email = @email, SupportRepId = @supportrepid WHERE CustomerID = {id}";
                    //string sql = $"UPDATE Customer SET State = @state WHERE CustomerID = {id}";
                    SqlCommand command = new SqlCommand(sql, connection);
                    
                    command.Parameters.AddWithValue("@firstname", customer.Firstname);
                    command.Parameters.AddWithValue("@lastname", customer.LastName);
                    command.Parameters.AddWithValue("@email", customer.Email);


                    if (customer.Company != null)
                    {
                        command.Parameters.AddWithValue("@company", customer.Company);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@company", DBNull.Value);
                    }

                    if (customer.Address != null)
                    {
                        command.Parameters.AddWithValue("@address", customer.Address);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@address", DBNull.Value);
                    }
                    
                    if (customer.City != null)
                    {
                        command.Parameters.AddWithValue("@city", customer.City);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@city", DBNull.Value);
                    }
                    
                    if (customer.Country != null)
                    {
                        command.Parameters.AddWithValue("@country", customer.Country);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@country", DBNull.Value);
                    }

                    if (customer.State != null)
                    {
                        command.Parameters.AddWithValue("@state", customer.State);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@state", DBNull.Value);
                    }
                    

                    if (customer.PostalCode != null)
                    {
                        command.Parameters.AddWithValue("@postalcode", customer.PostalCode);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@postalcode", DBNull.Value);
                    }

                    if (customer.Phone != null)
                    {
                        command.Parameters.AddWithValue("@phone", customer.Phone);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@phone", DBNull.Value);
                    }

                    if (customer.Fax != null)
                    {
                        command.Parameters.AddWithValue("@fax", customer.Fax);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@fax", DBNull.Value);

                    }

                    if (customer.SupportRepId != null)
                    {
                        command.Parameters.AddWithValue("@supportrepid", customer.SupportRepId);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@supportrepid", DBNull.Value);
                    }

                    command.ExecuteNonQuery();
                }

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
