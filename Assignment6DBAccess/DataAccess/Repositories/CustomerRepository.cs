﻿using Assignment6DBAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6DBAccess.DataAccess
{
    internal class CustomerRepository:ICustomerProgramRepository
    {
        IUserDataAccess _userDataAccess;

        public CustomerRepository(IUserDataAccess userDataAccess)
        {
            _userDataAccess = userDataAccess;
        }

        public bool CreateCustomer(Customer customer)
        { 
            bool result = _userDataAccess.CreateCustomer(customer);
            if(result)
            {
                return result;
            }
            else
            { 
                return false;
            }
        }

        public List<Customer> ReadAllCustomers()
        {
            return _userDataAccess.ReadAllCustomers();
        }

        public List<CustomerSpender> ReadAllCustomersByInvoiceAmount()
        {
            return _userDataAccess.ReadAllCustomersByInvoiceAmount();
        }

        public Customer ReadCustomerByID(int id)
        {
            return _userDataAccess.ReadCustomerByID(id);
        }

        public List<Customer> ReadCustomerByName(string name)
        {
            return _userDataAccess.ReadCustomerByName(name); ;
        }

        public List<Customer> ReadCustomerPage(int limit, int offset)
        {
            return _userDataAccess.ReadCustomerPage(limit, offset);
        }

        public List<CustomerCountry> ReadNumberOfCustomersByCountry()
        {
            return _userDataAccess.ReadNumberOfCustomersByCountry();
        }

        /// <summary>
        /// Retrieves all genres of the given customer, ordered descending.
        /// Checks wether the high most popular is the most popular and removes genres lower in popularity.
        /// </summary>
        /// <param name="customerId">Id of the customer whos genre to retrieve</param>
        /// <returns>A list of customer genres containing the information of the customer and the name of the genre.</returns>
        public List<CustomerGenre> ReadPopularGenreOfCustomerById(int customerId)
        {
            List <CustomerGenre> results = _userDataAccess.ReadPopularGenreOfCustomerById(customerId);

            for (int i = 0; i < results.Count; i++)
            {
                if(results[i].TrackTotal > results[i + 1].TrackTotal)
                {
                    results.RemoveRange(i + 1, results.Count - 1 - i);
                }
            }
            return results;
        }

        public bool UpdateCustomer(int id, Customer customer)
        {
            return _userDataAccess.UpdateCustomer(id, customer);
        }
    }
}
