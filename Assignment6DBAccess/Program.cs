﻿// See https://aka.ms/new-console-template for more information
using Assignment6DBAccess;
using Assignment6DBAccess.DataAccess;
using Assignment6DBAccess.DataAccess.SQL;
using Assignment6DBAccess.Models;

class Program
{
    
    public static void Main(string[] args)
    {
        IUserDataAccess userDataAccess = new UserSQLDataAccess();
        ICustomerProgramRepository customerRepo = new CustomerRepository(userDataAccess);

        UserController userController = new UserController();

        // 2.1 Read All customers
        userController.ReadAllCustomers();

        // 2.2 Read Customer by ID
        userController.ReadCustomersById(44);

        // 2.3 Read Customer by Name
        userController.ReadCustomerByName("Mar");

        // 2.4 Page of Customers
        userController.ReadCustomerPage(10, 25);

        // 2.5 Create Customer
        userController.CreateUser("Leroy", "Bemelen", "test@mail.com");

        // 2.6 Update Customer
        Customer customerToUpdate = new Customer("Leroy", "Bemelen", "test@mail.com");
        customerToUpdate.Id = 61;
        userController.UpdateUser(customerToUpdate);

        // 2.7 Read number customers by country
        userController.ReadNumberCustomersByCountry();

        // 2.8 Read all customers by invoice amount
        userController.ReadAllCustomersByInvoice();

        // 2.9 Read Popular Genres per Customer
        userController.ReadCustomerPopularGenre(12);
    }
}
